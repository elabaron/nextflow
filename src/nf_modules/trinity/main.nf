// SPDX-FileCopyrightText: 2022 Laurent Modolo <laurent.modolo@ens-lyon.fr>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

version = "2.13.2"
container_url = "lbmc/trinity:${version}"

params.sample = 3
params.min_glue = 1
params.min_contig_length = 200
params.assembly_out = ""


workflow assembly {
  take:
    fastq
  main:
    complete_assembly(fastq)
    super_transcript(complete_assembly.out.fasta)

  emit:
    fasta = complete_assembly.out.fasta
    super_transcript = super_transcript.out.fasta
}

process complete_assembly {
  container = "${container_url}"
  label "big_mem_multi_cpus"
  tag "$file_id"
  if (params.assembly_out != "") {
    publishDir "results/${params.assembly_out}", mode: 'copy'
  }

  input:
    tuple val(file_id), path(fastq)

  output:
    tuple val(file_id), path("trinity_output_${file_prefix}/"), emit: folder
    tuple val(file_id), path("trinity_output_${file_prefix}.Trinity.fasta"), emit: fasta
    tuple val(file_id), path("trinity_output_${file_prefix}.Trinity.fasta.gene_trans_map"), emit: gene_map 
    tuple val(file_id), path("trinity_output_${file_prefix}/salmon_outdir/quant.sf"), emit: quant

  script:

  switch(file_id) {
    case {it instanceof List}:
      file_prefix = file_id[0]
    break;
    case {it instanceof Map}:
      file_prefix = file_id.values()[0]
    break;
    default:
      file_prefix = file_id
    break;
  };
  def memory = "${task.memory}" - ~/\s*GB/

  if (fastq.size() == 2)
"""
  mkdir trinity_output_${file_prefix}
  Trinity \
    --seqType fq \
    --max_memory ${memory}G \
    --left ${fastq[0]} \
    --right ${fastq[1]} \
    --CPU ${task.cpus} \
    --min_glue ${params.min_glue} \
    --min_contig_length ${params.min_contig_length} \
    --output trinity_output_${file_prefix}
"""
  else
"""
  mkdir trinity_output_${file_prefix}
  Trinity \
    --seqType fq \
    --max_memory ${memory}G \
    --single ${fastq} \
    --CPU ${task.cpus} \
    --min_glue ${params.min_glue} \
    --min_contig_length ${params.min_contig_length} \
    --output trinity_output_${file_prefix}
"""
}


process super_transcript {
  container = "${container_url}"
  label "big_mem_mono_cpus"
  tag "$file_id"
  if (params.assembly_out != "") {
    publishDir "results/${params.assembly_out}", mode: 'copy'
  }

  input:
    tuple val(file_id), path(fasta)

  output:
    tuple val(file_id), path("trinity_genes.fasta"), path("trinity_genes.gtf"), emit: fasta

  script:

  switch(file_id) {
    case {it instanceof List}:
      file_prefix = file_id[0]
    break;
    case {it instanceof Map}:
      file_prefix = file_id.values()[0]
    break;
    default:
      file_prefix = file_id
    break;
  };
  def memory = "${task.memory}" - ~/\s*GB/

"""
Trinity_gene_splice_modeler.py \
  --trinity_fasta ${fasta}
"""
}
