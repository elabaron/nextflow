#!/bin/sh

# SPDX-FileCopyrightText: 2022 Laurent Modolo <laurent.modolo@ens-lyon.fr>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

docker pull lbmc/hicstuff:3.1.3
docker build src/.docker_modules/hicstuff/3.1.3/ -t 'lbmc/hicstuff:3.1.3'
docker push lbmc/hicstuff:3.1.3
#docker buildx build --platform linux/amd64,linux/arm64 -t "lbmc/hicstuff:3.1.3" --push src/.docker_modules/hicstuff/3.1.3/
