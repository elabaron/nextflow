#!/bin/sh

# SPDX-FileCopyrightText: 2022 Laurent Modolo <laurent.modolo@ens-lyon.fr>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

docker pull lbmc/samblaster:0.1.24
# docker build src/.docker_modules/samblaster/0.1.24 -t 'lbmc/samblaster:0.1.24'
# docker push lbmc/samblaster:0.1.24
docker buildx build --platform linux/amd64,linux/arm64 -t "lbmc/samblaster:0.1.24" --push src/.docker_modules/samblaster/0.1.24
