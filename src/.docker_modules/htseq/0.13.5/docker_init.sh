#!/bin/sh

# SPDX-FileCopyrightText: 2022 Laurent Modolo <laurent.modolo@ens-lyon.fr>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

docker pull lbmc/htseq:0.13.5
docker build src/.docker_modules/htseq/0.13.5 -t 'lbmc/htseq:0.13.5'
docker push lbmc/htseq:0.13.5
