#!/bin/sh
docker pull lbmc/plastid:0.5.1
docker build src/.docker_modules/plastid/0.5.1 -t 'lbmc/plastid:0.5.1'
docker push lbmc/plastid:0.5.1
